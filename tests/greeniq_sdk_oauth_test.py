import unittest
from tests.greeniq_sdk_test import GreenIQTest
from gardenkit import GardenKit
from urllib.error import HTTPError

class GreenIQOAuthTest(GreenIQTest):
    
    def test_login(self):
        gardenkit = self.get_gardenkit_instance()
        gardenkit.login()
        self.assertEqual('ZDFkYTE3NTAtZGUyMC00OTQ5LWE2MzYtNWU3NTNkOWIwZjI5', gardenkit.access_token, 'Unexpected user token') 
    
    def test_login_wrong_password(self):
        try:
            gardenkit = GardenKit("SHOULD_NOTexist","SHOULD_NOTexist",client_id='greeniq',client_secret='this_is_not_a_secret', domain=GreenIQTest.DOMAIN)
            gardenkit.login()
            self.assertFalse(True,"Login should not have passed." )
        except HTTPError as e1:
            #all is well
            assert e1.code == 401

    def get_gardenkit_instance(self):
        return GardenKit(GreenIQTest.USERNAME,GreenIQTest.PASSWORD,client_id='greeniq',client_secret='this_is_not_a_secret', domain=GreenIQTest.DOMAIN)
    
if __name__ == '__main__':
    unittest.main()
    #suiteFew = unittest.TestSuite()
    #suiteFew.addTest(GreenIQOAuthTest("test_login_wrong_password"))
    #unittest.TextTestRunner(verbosity=2).run(suiteFew)