import unittest,time,datetime
from xml.dom import minidom
from gardenkit import GardenKit, GardenKitException
import calendar

class GreenIQTest(unittest.TestCase):
    
    USERNAME = 'GREENIQDEV';
    PASSWORD = 'GREENIQDEV';
    DOMAIN ='146.148.10.206'
    
    def test_login(self):
        gardenkit = self.get_gardenkit_instance()
        gardenkit.login()
        self.assertEqual('798978adfbf4f0a95762106e9881e66ad1e88c455d0eae8530fb963b', gardenkit.access_token, 'Unexpected user token') 

    def test_login_wrong_password(self):
        try:
            gardenkit = GardenKit("SHOULD_NOTexist","SHOULD_NOTexist", domain=GreenIQTest.DOMAIN)
            gardenkit.login()
            self.assertFalse(True,"Login should not have passed." )
        except GardenKitException:
            #all is well
            pass
        
    def test_get_set_valves_config(self):
        gardenkit = self.get_gardenkit_instance()
        gardenkit.login()
        gardenkit.set_valves_configuration(False, [])
        time.sleep(90)
        res =  gardenkit.get_valves_configuration_and_status()
        self.assertFalse(res['master'],res)
        ports = res['ports']
        port1 = ports[0]
        port2 = ports[1]
        port3 = ports[2]
        port4 = ports[3]        
        port5 = ports[4]                            
        port6 = ports[5]
        self.assertEqual(port1['number'], 1)
        self.assertEqual(port2['number'], 2)
        self.assertEqual(port3['number'], 3)
        self.assertEqual(port4['number'], 4)
        self.assertEqual(port5['number'], 5)
        self.assertEqual(port6['number'], 6)

        self.assertEqual(port1['active'], False)
        self.assertEqual(port2['active'], False)
        self.assertEqual(port3['active'], False)
        self.assertEqual(port4['active'], False)
        self.assertEqual(port5['active'], False)
        self.assertEqual(port6['active'], False)
        
        gardenkit.set_valves_configuration(True, [{'number':1,'configuration':True},
                                                  {'name':'area2','configuration':True},
                                                  {'number':3,'configuration':'Auto'},
                                                  {'number':4,'configuration':True},
                                                  {'number':5,'configuration':True},
                                                  {'number':6,'configuration':False}])
        
        time.sleep(90) #give the hub some time to pull configuration and then some additional time to push status to the cloud.
        res =  gardenkit.get_valves_configuration_and_status()

        self.assertTrue(res['master'],res)
        ports = res['ports']
        port1 = ports[0]
        port2 = ports[1]
        port3 = ports[2]
        port4 = ports[3]        
        port5 = ports[4]                            
        port6 = ports[5]
        self.assertEqual(port1['number'], 1)
        self.assertEqual(port2['number'], 2)
        self.assertEqual(port3['number'], 3)
        self.assertEqual(port4['number'], 4)
        self.assertEqual(port5['number'], 5)
        self.assertEqual(port6['number'], 6)

        self.assertEqual(port1['active'], True)
        self.assertEqual(port2['active'], True)
        self.assertEqual(port3['active'], False)
        self.assertEqual(port4['active'], True)
        self.assertEqual(port5['active'], True)
        self.assertEqual(port6['active'], False)

    def test_get_set_light_config(self):
        gardenkit = self.get_gardenkit_instance()
        gardenkit.login()
        gardenkit.set_light_configuration(False, [])
        #time.sleep(60)
        res =  gardenkit.get_light_configuration_and_status()
        self.assertFalse(res['master'],res)        
        gardenkit.set_light_configuration(True, [])
        #time.sleep(60)
        res =  gardenkit.get_light_configuration_and_status()
        self.assertTrue(res['master'],res)        

        
        #gardenkit.set_valves_configuration(True, [{'number':1,'configuration':False},{'name':'area2','configuration':True},{'number':4,'configuration':'Auto'}])
    def test_get_hub_details(self):
        gardenkit = self.get_gardenkit_instance()
        gardenkit.login()
        res = gardenkit.get_hub_status()
        assert 'version' in res
        assert 'ssid' in res
        
    def test_update_program(self):
        gardenkit = self.get_gardenkit_instance()
        gardenkit.login()
        gardenkit.update_program(5,3, True, True,datetime.time(12,45),datetime.time(17,32),days={calendar.SUNDAY:False})
        xmlasstr = gardenkit.get_config()
        self.assertTrue('17:32' in xmlasstr,'Expecting time string in returned config')
        
        gardenkit.update_program(6,1, False, True,datetime.time(10,37),datetime.time(19,59),
                                 days={calendar.SUNDAY:False,
                                       calendar.MONDAY:True,
                                       calendar.TUESDAY:False,
                                       calendar.WEDNESDAY:True,
                                       calendar.THURSDAY:False,
                                       calendar.FRIDAY:True,
                                       calendar.SATURDAY:False})
        
        xmlasstr = gardenkit.get_config()
        self.assertTrue('0101010' in xmlasstr)
        self.assertTrue('10:37' in xmlasstr,'Expecting time string in returned config')

    def test_update_program_interval(self):
        gardenkit = self.get_gardenkit_instance()
        gardenkit.login()
        gardenkit.update_program(5,3, True, True,datetime.time(12,45),datetime.time(17,32),interval={'every_x_days':3,'starting':datetime.date(2025,10,1)})
        xmlasstr = gardenkit.get_config()
        self.assertTrue('<year>2025</year>' in xmlasstr,'Expecting time string in returned config')
        
    def test_get_config(self):
        gardenkit = self.get_gardenkit_instance()
        gardenkit.login()
        xmlasstr = gardenkit.get_config()
        minidom.parseString(xmlasstr)
        
    def get_gardenkit_instance(self):
        return GardenKit(GreenIQTest.USERNAME,GreenIQTest.PASSWORD, domain=GreenIQTest.DOMAIN)
if __name__ == '__main__':
    unittest.main()
    #suiteFew = unittest.TestSuite()
    #suiteFew.addTest(GreenIQTest("test_update_program_interval"))
    #unittest.TextTestRunner(verbosity=2).run(suiteFew)