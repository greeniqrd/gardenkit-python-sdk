GreenIQ revolutionise gardening with the Smart Garden Hub and cuts outdoor water consumption by 50%. 
 
The GardenKit API is a REST API which gives the developer all the components that are required to build a full or partial app for the Smart Garden Hub. 

The garden kit python package is a python3 SDK which abstracts the work with the rest API.

**GardenKit API now works with oAuth2.0** Contact us to get your client ID and secret 

**Having questions?** Email us to developers@greeniq.co

**Supported python version**: 3.X

**Installing the gardenkit**: pip3 install gardenkit

[Click here to see an example script](https://bitbucket.org/greeniqrd/gardenkit-python-sdk/src/fda6384093cd85108b41f98e98f0c0bcf4eed504/examples/greeniq_party.py)