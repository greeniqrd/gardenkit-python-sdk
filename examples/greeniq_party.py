import time,datetime
from gardenkit import GardenKit

'''
Example script which does the following
    1. Turns all irrigation and lightning on 
    2. Shuts down all irrigation & lightning
    3. Activates  programs for areas 1
    4. Polls irrigation status & configuration.
'''

greeniq_api = GardenKit("GREENIQDEV","GREENIQDEV",client_id='greeniq',client_secret='this_is_not_a_secret',domain='greeniq.net')
greeniq_api.login()

hub_status = greeniq_api.get_hub_status()
last_seen = datetime.datetime.fromtimestamp(int(hub_status['last_seen'])).strftime('%Y-%m-%d %H:%M:%S')
print('Running example script on %s - HUB version %s, Last seen %s' %(hub_status['ssid'],hub_status['version'],last_seen))
print()
print('Turning all irrigation and lightning')
greeniq_api.set_valves_configuration(True, [{'number':1,'configuration':True},
                                            {'name':'area2','configuration':True},
                                            {'number':3,'configuration':True},
                                            {'number':4,'configuration':True},
                                            {'number':5,'configuration':True},
                                            {'number':6,'configuration':True}])

greeniq_api.set_light_configuration(True,  [{'number':8,'configuration':True}])
print('60 seconds until hub is updates with configuration + 2 minutes letting irrigation run')
time.sleep(180)

print('Now turning everything off')
greeniq_api.set_valves_configuration(True, [{'number':1,'configuration':False},
                                            {'name':'area2','configuration':False},
                                            {'number':3,'configuration':False},
                                            {'number':4,'configuration':False},
                                            {'number':5,'configuration':False},
                                            {'number':6,'configuration':False}])

greeniq_api.set_light_configuration(True,  [{'number':8,'configuration':False}])
time.sleep(70)

print('Activating programs')
greeniq_api.set_valves_configuration(True, [{'number':1,'configuration':'Auto'}])

greeniq_api.update_program(1, 1,True,False,datetime.time(0,0),datetime.time(23,59),interval={'every_x_days':1,'starting':datetime.date(2012,10,1)})
greeniq_api.update_program(1, 2,False)
greeniq_api.update_program(1, 3,False)
greeniq_api.update_program(1, 4,False)

for i in range(10):
    res = greeniq_api.get_valves_configuration_and_status()
    print(res)
    time.sleep(10)
